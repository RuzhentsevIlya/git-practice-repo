namespace example 
{

template <typename T>
struct adder
{
	adder(T x) : _x(x) {}
	void operator()(T& e) const { e += _x; }
private:
	T _x;
};

template <typename T>
struct greater_then
{
	greater_then(T x) : value(x) {}
	bool operator()(T e) const { return e > value; }
private:
	T value;
};

}
